//
//  ViewController.swift
//  Animations
//
//  Created by EuvicDev on 02/08/2017.
//  Copyright © 2017 EuvicDev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK: Outlets

    @IBOutlet weak var btnToAnimate: UIButton!
    @IBOutlet weak var btnToAnimateConstraint: NSLayoutConstraint!
    @IBOutlet weak var btn2ToAnimateConstraint: NSLayoutConstraint!
    @IBOutlet weak var btn3ToAnimateConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnToAnimateConstraint.constant -= view.bounds.width
        btn2ToAnimateConstraint.constant -= view.bounds.width
        // Do any additional setup after loading the view, typically from a nib.
    }

    var isAnimationPerformed = false
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if !isAnimationPerformed {
            UIView.animate(withDuration: 0.7, delay: 0, options: .curveEaseOut, animations: {
                self.btnToAnimateConstraint.constant += self.view.bounds.width
                self.view.layoutIfNeeded()
            }, completion: nil)

            UIView.animate(withDuration: 0.7, delay: 0.07, options: .curveEaseInOut, animations: {
                self.btn2ToAnimateConstraint.constant += self.view.bounds.width
                self.view.layoutIfNeeded()
            }, completion: nil)
            isAnimationPerformed = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

